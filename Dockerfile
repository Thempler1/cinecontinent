FROM node:8.6
WORKDIR  /app
COPY ./ /app/
RUN npm install
EXPOSE 4200
ENTRYPOINT npm start