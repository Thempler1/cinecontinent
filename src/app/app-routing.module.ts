import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CarteleraComponent } from './cartelera/cartelera.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { BoletoComponent } from './boleto/boleto.component';


const routes: Routes = [
  {path:'', component: HomeComponent},
  {path: 'cartelera', component: CarteleraComponent},
  {path: 'administracion', component: AdministracionComponent},
  {path: 'estadisticas', component: EstadisticasComponent},
  {path: 'boleto', component: BoletoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
