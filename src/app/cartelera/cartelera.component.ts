import { Component, OnInit } from '@angular/core';
import { MovieDataService } from '../services/movie-data.service';
import { CinemaDataService } from '../services/cinema-data.service';
import { Cinema } from '../models/cinema';

@Component({
  selector: 'app-cartelera',
  templateUrl: './cartelera.component.html',
  styleUrls: ['./cartelera.component.scss']
})
export class CarteleraComponent implements OnInit {

  movieArr: any;
  cinemaArr: any;

  constructor(private movieDataService: MovieDataService , private cinemaDataService: CinemaDataService) { 
    this.movieArr = movieDataService.moviesArray;
    this.cinemaArr = cinemaDataService.cinemaArray;
  }

  ngOnInit() {
  }

}
