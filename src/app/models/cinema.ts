import { Movie } from '../models/movie';

export class Cinema {
    id: number = 0;
    tipo: string;
    sillasTotal: 20;
    sillasDisponibles: 20;
    fechaHora: string ="11/marzo/2018-00:00";
    pelicula: Movie;
}
