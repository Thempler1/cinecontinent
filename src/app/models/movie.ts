export class Movie {
    id: number = 0;
    nombre: string;
    idioma: string;
    edadEstablecida: number;
    director: string;

}