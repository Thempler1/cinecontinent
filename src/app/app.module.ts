import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { CarteleraComponent } from './cartelera/cartelera.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { MovieDataService } from './services/movie-data.service';
import { CinemaDataService } from './services/cinema-data.service';
import { BoletoComponent } from './boleto/boleto.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CarteleraComponent,
    AdministracionComponent,
    EstadisticasComponent,
    BoletoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [MovieDataService,CinemaDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
