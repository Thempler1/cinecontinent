import { Component, OnInit } from '@angular/core';
import { Movie } from '../models/movie';
import { MovieDataService } from '../services/movie-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  movieArr: any;

  constructor(private movieDataService: MovieDataService) { 
    this.movieArr = movieDataService.moviesArray;
  }

  ngOnInit() {
  }

}
