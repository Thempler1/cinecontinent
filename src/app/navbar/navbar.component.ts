import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  appTitle: string = 'CineContinent'

  constructor() { }

  ngOnInit() {
  }

}
export class NavComponent implements OnInit { 
  ; // OR (either will work) appTitle = 'myapp'; 
  constructor() { } ngOnInit() { } }