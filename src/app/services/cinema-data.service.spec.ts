import { TestBed } from '@angular/core/testing';

import { CinemaDataService } from './cinema-data.service';

describe('CinemaDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CinemaDataService = TestBed.get(CinemaDataService);
    expect(service).toBeTruthy();
  });
});
