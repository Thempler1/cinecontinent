import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';
import { MovieDataService } from '../services/movie-data.service';
import { Cinema } from '../models/cinema';

@Injectable({
  providedIn: 'root'
})
export class CinemaDataService {

  cinemaArray: Cinema []=[
    {id: 1,tipo: "Sala 5D",sillasTotal: 20 ,sillasDisponibles: 20 ,fechaHora:"11/marzo/2018-00:00", pelicula: {id: 1 , nombre:"MIB: Hombres de Negro", idioma:"Español Latino", edadEstablecida: 8, director: "F. Gary Gray"}},
    {id: 1,tipo: "Sala 3D",sillasTotal: 20 ,sillasDisponibles: 20 ,fechaHora:"11/marzo/2018-00:00", pelicula: {id: 2 , nombre:"Godzilla II: El rey de los mounstruos", idioma:"Español Latino", edadEstablecida: 13, director: "Michael Dougherty"}},
    {id: 1,tipo: "Sala Imax",sillasTotal: 20 ,sillasDisponibles: 20 ,fechaHora:"11/marzo/2018-00:00", pelicula: {id: 3 , nombre:"X-MEN: Dark Phoenix", idioma:"Subtitulada Español", edadEstablecida: 13, director: "Simon Kinberg"}},
    {id: 1,tipo: "Sala Simple",sillasTotal: 20 ,sillasDisponibles: 20 ,fechaHora:"11/marzo/2018-00:00", pelicula: {id: 3 , nombre:"X-MEN: Dark Phoenix", idioma:"Subtitulada Español", edadEstablecida: 13, director: "Simon Kinberg"}}
  ]

  constructor() { }
}
