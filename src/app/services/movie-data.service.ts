import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieDataService {

  selectedMovie: Movie = new Movie();

  moviesArray: Movie []=[
    {id: 1 , nombre:"MIB: Hombres de Negro", idioma:"Español Latino", edadEstablecida: 8, director: "F. Gary Gray"},
    {id: 2 , nombre:"Godzilla II: El rey de los mounstruos", idioma:"Español Latino", edadEstablecida: 13, director: "Michael Dougherty"},
    {id: 3 , nombre:"X-MEN: Dark Phoenix", idioma:"Subtitulada Español", edadEstablecida: 13, director: "Simon Kinberg"},
    {id: 4 , nombre:"-Prueba de funcionamiento-", idioma:"x", edadEstablecida: 1, director: "x"}
  ]

  addMovie(){
    if (this.selectedMovie.id === 0){
      this.selectedMovie.id=this.moviesArray.length + 1;
      this.moviesArray.push(this.selectedMovie)
    }
  }

  constructor() { }

}
